drupal_get_project_version() {
	if [ $# != 1 ]; then
		error_single_argument_expected 'drupal_get_project_version'
	fi
	# shellcheck disable=SC2039
	local project_path project_version
	project_path="$1"
	project_version=$(get_package_version_from_composer "$project_path" 'drupal/core')
	printf '%s' "$project_version"
	return 0
}
