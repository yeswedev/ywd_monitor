error_single_argument_expected() {
	if [ $# != 1 ]; then
		error_single_argument_expected 'error_single_argument_expected'
	fi
	# shellcheck disable=SC2039
	local caller
	caller="$1"
	printf 'Error:\n' >&2
	printf '%s requires exactly one argument.\n' "$caller" >&2
	return 1
}
error_missing_required_file() {
	if [ $# != 1 ]; then
		error_single_argument_expected 'error_missing_required_file'
	fi
	# shellcheck disable=SC2039
	local file
	file="$1"
	printf 'Error:\n' >&2
	printf 'Required file not found: %s\n' "$(realpath "$file")" >&2
	return 1
}
error_wrong_arguments_number() {
	if [ $# != 3 ]; then
		error_wrong_arguments_number 'error_wrong_arguments_number' 3 $#
	fi
	# shellcheck disable=SC2039
	local caller number_expected number_given
	caller="$1"
	number_expected="$2"
	number_given="$3"
	printf 'Error:\n' >&2
	printf '%s got a wrong number of arguments.\n' "$caller" >&2
	printf 'Expected number: %s\n' "$number_expected" >&2
	printf 'Given number: %s\n' "$number_given" >&2
	return 1
}
