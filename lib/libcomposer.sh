get_package_version_from_composer() {
	if [ $# != 2 ]; then
		error_wrong_arguments_number 'get_package_version_from_composer' 2 $#
	fi
	# shellcheck disable=SC2039
	local project_path package_name package_version
	project_path="$1"
	package_name="$2"
	if [ ! -e "$project_path/composer.lock" ]; then
		error_missing_required_file "$project_path/composer.lock"
	fi
	if grep --quiet "\"name\": \"$package_name\"" "$project_path/composer.lock"; then
		package_version=$(trim_quotes "$(jq '.packages[]' "$project_path/composer.lock"  | jq '.name,.version' | grep --after 1 "$package_name" | head --lines=2 | tail --lines=1)")
	else
		package_version='unknown'
	fi
	printf '%s' "$package_version"
	return 0
}
