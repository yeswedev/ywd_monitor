wordpress_get_project_version() {
	if [ $# != 1 ]; then
		error_single_argument_expected 'wordpress_get_project_version'
	fi
	# shellcheck disable=SC2039
	local project_path project_version
	project_path="$1"
	project_version=$(get_package_version_from_composer "$project_path" 'johnpbloch/wordpress')
	if [ "$project_version" = 'unknown' ]; then
		project_version=$(get_package_version_from_composer "$project_path" 'roots/wordpress')
	fi
	printf '%s' "$project_version"
	return 0
}
