get_project_type() {
	if [ $# != 1 ]; then
		error_single_argument_expected 'get_project_type'
	fi
	# shellcheck disable=SC2039
	local project_path project_type
	project_path="$1"
	if [ -f "$project_path/ywd_deploy/toolbox.conf" ]; then
		project_type=$(sed --silent "/^PROJECT_TECH=/s/.*='\\(.*\\)'/\\1/p" "$project_path/ywd_deploy/toolbox.conf")
	elif [ -f "$project_path/ywd_toolbox/toolbox.conf" ]; then
		project_type=$(sed --silent "/^PROJECT_TECH=/s/.*='\\(.*\\)'/\\1/p" "$project_path/ywd_toolbox/toolbox.conf")
	else
		project_type=$(guess_project_type "$project_path")
	fi
	if [ -z "$project_type" ]; then
		project_type='unknown'
	fi
	printf '%s' "$project_type"
	return 0
}
guess_project_type() {
	if [ $# != 1 ]; then
		error_single_argument_expected 'guess_project_type'
	fi
	# shellcheck disable=SC2039
	local project_path project_type
	project_path="$1"
	if \
		[ -f "$project_path/composer.json" ] && \
		grep --quiet 'laravel/framework' "$project_path/composer.json"
	then
		project_type='laravel'
	elif \
		[ -f "$project_path/composer.json" ] && \
		grep --quiet 'laravel/lumen-framework' "$project_path/composer.json"
	then
		project_type='lumen'
	elif \
		[ -f "$project_path/composer.json" ] && \
		grep --quiet --regexp='johnpbloch/wordpress' --regexp='roots/wordpress'  "$project_path/composer.json"
	then
		project_type='wordpress'
	elif \
		[ -f "$project_path/composer.json" ] && \
		grep --quiet --regexp='drupal-composer/drupal-scaffold' "$project_path/composer.json"
	then
		project_type='drupal_composer'
	elif \
		[ -f "$project_path/version.php" ] && \
		grep --quiet --regexp='// This file is part of Moodle - http://moodle.org/' "$project_path/version.php"
	then
		project_type='moodle'
	elif [ -f "$project_path/wp-settings.php" ]; then
		project_type='wordpress_vanilla'
	elif [ -f "$project_path/app/config/parameters.php" ]; then
		project_type='prestashop'
	elif [ -f "$project_path/config/settings.inc.php" ]; then
		project_type='prestashop1.6'
	else
		# shellcheck disable=SC2039
		local file
		for file in "$project_path/sites/"*"/settings.php"; do
			if [ -e "$file" ]; then
				project_type='drupal'
			fi
		done
	fi
	if [ -z "$project_type" ]; then
		project_type='unknown'
	fi
	printf '%s' "$project_type"
	return 0
}
get_project_version() {
	if [ $# != 1 ]; then
		error_single_argument_expected 'get_project_version'
	fi
	# shellcheck disable=SC2039
	local project_path project_type project_version
	project_path="$1"
	project_type=$(get_project_type "$project_path")
	case "$project_type" in
		('drupal_composer')
			project_version=$(drupal_get_project_version "$project_path")
		;;
		('laravel')
			project_version=$(laravel_get_project_version "$project_path")
		;;
		('lumen')
			project_version=$(lumen_get_project_version "$project_path")
		;;
		('wordpress')
			project_version=$(wordpress_get_project_version "$project_path")
		;;
		(*)
			project_version='unknown'
		;;
	esac
	printf '%s' "$project_version"
	return 0
}
trim_quotes() {
	if [ $# != 1 ]; then
		error_single_argument_expected 'trim_quotes'
	fi
	# shellcheck disable=SC2039
	local string
	string="$1"
	if [ "${string%\"}" != "$string" ] && [ "${string#\"}" != "$string" ]; then
		string="${string%\"}"
		string="${string#\"}"
	elif [ "${string%\'}" != "$string" ] && [ "${string#\'}" != "$string" ]; then
		string="${string%\'}"
		string="${string#\'}"
	fi
	printf '%s' "$string"
	return 0
}
