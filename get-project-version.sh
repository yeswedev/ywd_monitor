#!/bin/sh
set -o errexit

APP_DIR=$(dirname "$(realpath "$0")")

if [ ! -e "$APP_DIR/libmonitor.sh" ]; then
	printf 'Error:\n' >&2
	printf 'Required library not found: %s\n' "$APP_DIR/libmonitor.sh" >&2
	printf 'You can build it running: %s\n' "cd '$APP_DIR' && make" >&2
	exit 1
fi

# shellcheck source=libmonitor.sh
. "$APP_DIR/libmonitor.sh"

if [ $# != 1 ]; then
	error_single_argument_expected "$(basename "$0")"
fi

PROJECT_PATH="$1"
PROJECT_VERSION=$(get_project_version "$PROJECT_PATH")

printf '%s' "$PROJECT_VERSION"

exit 0
