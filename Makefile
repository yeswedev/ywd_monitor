all: libmonitor.sh

libmonitor.sh: lib/*
	cat lib/* > libmonitor.sh

clean:
	rm -f libmonitor.sh
